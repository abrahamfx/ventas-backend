package com.gutierrez.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gutierrez.model.Producto;

@Repository
public interface ProductoDAO extends JpaRepository<Producto, Integer>{

}
