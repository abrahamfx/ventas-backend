package com.gutierrez.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gutierrez.model.Venta;

@Repository
public interface VentaDAO extends JpaRepository<Venta, Integer>{

}
