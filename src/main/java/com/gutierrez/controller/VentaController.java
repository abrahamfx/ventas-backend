package com.gutierrez.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gutierrez.model.Venta;
import com.gutierrez.service.IVenta;

@RestController
@RequestMapping("/venta")
public class VentaController {

	@Autowired
	private IVenta service;

	@PostMapping(value = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Venta> registrar(@RequestBody Venta ven) {
		Venta venta = new Venta();
		try {
			venta = service.registrar(ven);
		} catch (Exception e) {
			return new ResponseEntity<Venta>(venta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Venta>(venta, HttpStatus.OK);
	}

	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Venta>> listar() {
		List<Venta> venta = new ArrayList<>();
		try {
			venta = service.listar();
		} catch (Exception e) {
			return new ResponseEntity<List<Venta>>(venta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Venta>>(venta, HttpStatus.OK);
	}
}
