package com.gutierrez.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gutierrez.model.DetalleVenta;
import com.gutierrez.service.IDetalleVenta;

@RestController
@RequestMapping("/detalleventa")
public class DetalleVentaController {

	private IDetalleVenta service;

	@PostMapping(value = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DetalleVenta> registrar(@RequestBody DetalleVenta detalle) {
		DetalleVenta detalleVenta = new DetalleVenta();
		try {
			detalleVenta = service.registrar(detalle);
		} catch (Exception e) {
			return new ResponseEntity<DetalleVenta>(detalleVenta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<DetalleVenta>(detalleVenta, HttpStatus.OK);
	}

	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<DetalleVenta>> listar() {
		List<DetalleVenta> detalles = new ArrayList<>();
		try {
			detalles = service.listar();
		} catch (Exception e) {
			return new ResponseEntity<List<DetalleVenta>>(detalles, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<DetalleVenta>>(detalles, HttpStatus.OK);
	}

}
