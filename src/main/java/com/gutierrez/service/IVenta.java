package com.gutierrez.service;

import java.util.List;

import com.gutierrez.model.Venta;

public interface IVenta {

	Venta registrar(Venta venta);

	List<Venta> listar();

}
