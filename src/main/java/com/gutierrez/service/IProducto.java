package com.gutierrez.service;

import java.util.List;

import com.gutierrez.model.Producto;

public interface IProducto {

	Producto registrar(Producto producto);

	Producto modificar(Producto producto);

	List<Producto> listar();

	Producto listarId(int idProducto);

	void eliminar(int idProducto);

}
