package com.gutierrez.service;

import java.util.List;

import com.gutierrez.model.DetalleVenta;

public interface IDetalleVenta {

	DetalleVenta registrar(DetalleVenta detalleVenta);
	List<DetalleVenta> listar();
}
