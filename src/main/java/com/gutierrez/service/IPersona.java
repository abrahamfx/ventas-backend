package com.gutierrez.service;

import java.util.List;

import com.gutierrez.model.Persona;

public interface IPersona {

	Persona registrar(Persona persona);
	Persona modificar(Persona persona);
	List<Persona> listar();
	Persona listarId(int idPersona);
	void eliminar(int idPersona);
}
