package com.gutierrez.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gutierrez.dao.ProductoDAO;
import com.gutierrez.model.Producto;
import com.gutierrez.service.IProducto;

@Service
public class ProductoImpl implements IProducto {

	@Autowired
	private ProductoDAO dao;

	@Override
	public Producto registrar(Producto producto) {
		return dao.save(producto);
	}

	@Override
	public Producto modificar(Producto producto) {
		return dao.save(producto);
	}

	@Override
	public List<Producto> listar() {
		return dao.findAll();
	}

	@Override
	public Producto listarId(int idProducto) {
		return dao.findOne(idProducto);
	}

	@Override
	public void eliminar(int idProducto) {
		dao.delete(idProducto);

	}

}
