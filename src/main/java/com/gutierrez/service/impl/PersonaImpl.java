package com.gutierrez.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gutierrez.dao.PersonaDAO;
import com.gutierrez.model.Persona;
import com.gutierrez.service.IPersona;

@Service
public class PersonaImpl implements IPersona {

	@Autowired
	private PersonaDAO dao;

	@Override
	public Persona registrar(Persona persona) {
		return dao.save(persona);
	}

	@Override
	public Persona modificar(Persona persona) {
		return dao.save(persona);
	}

	@Override
	public List<Persona> listar() {
		return dao.findAll();
	}

	@Override
	public Persona listarId(int idPersona) {
		return dao.findOne(idPersona);
	}

	@Override
	public void eliminar(int idPersona) {
		dao.delete(idPersona);

	}

}
