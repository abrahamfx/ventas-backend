package com.gutierrez.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gutierrez.dao.DetalleVentaDAO;
import com.gutierrez.model.DetalleVenta;
import com.gutierrez.service.IDetalleVenta;

@Service
public class DetalleVentaImpl implements IDetalleVenta {

	@Autowired
	private DetalleVentaDAO dao;

	@Override
	public DetalleVenta registrar(DetalleVenta detalleVenta) {
		return dao.save(detalleVenta);
	}

	@Override
	public List<DetalleVenta> listar() {
		return dao.findAll();
	}

}
