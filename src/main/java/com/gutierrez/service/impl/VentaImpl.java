package com.gutierrez.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gutierrez.dao.VentaDAO;
import com.gutierrez.model.Venta;
import com.gutierrez.service.IVenta;

@Service
public class VentaImpl implements IVenta {

	@Autowired
	private VentaDAO dao;

	@Override
	public Venta registrar(Venta venta) {
		return dao.save(venta);
	}

	@Override
	public List<Venta> listar() {
		return dao.findAll();
	}

}
